#!/bin/sh
# Jake Yeung
# run_make_trackhubs.sh
# Run trackhubs and upload in batch then clean up  
# 2016-03-24

pyscript="/Home/jyeung/projects/make_trackhub_scripts/make_trackhub_4cseq/make_trackhubs.py"
pyscriptagg="/Home/jyeung/projects/make_trackhub_scripts/make_trackhub_4cseq/make_trackhubs.aggregate.py"

[[ ! -e $pyscript ]] && echo "$pyscript not found, exiting" && exit 1
[[ ! -e $pyscriptagg ]] && echo "$pyscriptagg not found, exiting" && exit 1

# dirmain=$1
dirmain="/archive/epfl/upnae/jyeung/4c_seq/trackhub_data/4cseq_sigma2500"
dirmainbase=$(basename $dirmain)

hubdirmain="/scratch/el/monthly/jyeung/trackhub_tmp2"
[[ ! -d $hubdirmain ]] && mkdir $hubdirmain

# hubdirname=$2
hubdirname="sigma2500_with_aggregate"
hubdir=$hubdirmain/$hubdirname
[[ ! -d $hubdir ]] && mkdir $hubdir
cd $hubdir


[[ ! -e $pyscript ]] && echo "$pyscript not found, exiting" && exit 1
[[ ! -d $dirmain ]] && echo "$dirmain not found, exiting" && exit 1

for d in `ls -d $dirmain/pseudo*`; do
	dbase=$(basename $d)
	# echo $dbase
	# rearrange
	dbase=`echo $dbase | awk -F . 'BEGIN {OFS=FS} {print $3, $1, $2}'`
	echo "python $pyscript $d $dbase $dirmainbase --render --upload"
done

# do aggregate on log signal
logsigdir=`ls -d $dirmain/*.logsignal`
echo "python $pyscriptagg $logsigdir "aggregate.log.signal" $dirmainbase --render --upload"

# clean up 
# rm $hubdir/*.txt
# rm $hubdir/mm9/trackDb.txt
# rmdir $hubdir/mm9
