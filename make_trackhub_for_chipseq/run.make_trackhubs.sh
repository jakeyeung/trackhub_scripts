#!/bin/sh
# Jake Yeung
# run.make_trackhubs.sh
#  
# 2017-04-05

runscript="/Home/jyeung/projects/make_trackhub_scripts/make_trackhub_for_chipseq/make_trackhubs.py"
[[ ! -e $runscript ]] && echo "$runscript not found, exiting" && exit 1
dirmain="/archive/epfl/upnae/jyeung/chipseq_data/bigwigs_tissue_and_clock"
[[ ! -d $dirmain ]] && echo "$dirmain not found, exiting" && exit 1

python $runscript $dirmain --render --upload
