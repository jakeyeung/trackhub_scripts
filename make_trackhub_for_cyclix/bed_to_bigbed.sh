#!/bin/sh
# Jake Yeung
# bed_to_bigbed.sh
# Convert bed to bigbed 
# WARNING: removes first two lines using AWK without checking
# 2016-05-04

# indir="/scratch/el/monthly/jyeung/trackhub_files"
# outdir="/scratch/el/monthly/jyeung/trackhub_files/bigbeds"
# indir=$1
# outdir=$2
indir="/archive/epfl/upnae/jyeung/cyclix/amp_phase_beds"
outdir="/archive/epfl/upnae/jyeung/cyclix/amp_phase_beds/bigbeds"

[[ ! -d $indir ]] && echo "$indir not found, exiting" && exit 1
[[ ! -d $outdir ]] && mkdir $outdir

tmpdir="/tmp"
genome="mm9"
chromsizes=$tmpdir/chromsizes.$genome

[[ ! -d $indir ]] && echo "$indir not found, exiting" && exit 1
[[ ! -d $outdir ]] && echo "$outdir not found, exiting" && exit 1

fetchChromSizes $genome > $chromsizes

for b in `ls -d $indir/*.bed`; do
	bname=$(basename $b)
	noext=${bname%.*}
	awk 'NR > 2' $b | sort -k1,1 -k2,2n  > $tmpdir/$noext
	bedToBigBed $tmpdir/$noext $chromsizes $outdir/$noext.bb
done
