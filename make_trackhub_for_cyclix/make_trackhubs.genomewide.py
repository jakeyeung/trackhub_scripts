#!/usr/bin/env python
'''
DESCRIPTION

    Make trackhubs for UCSC browser using bigbed files.
    Tutorials for making trackhubs with trackhub python package from:
    https://pythonhosted.org/trackhub/tutorial.html#creating-a-composite-track

    Motevo Motif sitecounts: bigbed files

FOR HELP

    python make_trackhubs.py --help

AUTHOR:      Jake Yeung (jake.yeung@epfl.ch)
LAB:         Computational Systems Biology Lab (http://naef-lab.epfl.ch)
CREATED ON:  2016-01-06
LAST CHANGE: see git log
LICENSE:     MIT License (see: http://opensource.org/licenses/MIT)

EXAMPLE

make_trackhubs.py /archive/epfl/upnae/jyeung/cyclix/amp_phase_beds/bigbeds
'''

import sys, argparse, os
from trackhub import Hub, GenomesFile, Genome, TrackDb, Track
from trackhub import CompositeTrack
from trackhub.track import SubGroupDefinition
from trackhub import ViewTrack

from trackhub.upload import upload_track, upload_hub

# import numpy as np

def get_files_from_dir(inputdir, ext=".bb"):
    '''
    Get *.bb files
    Output as dic with {name: name.bb}
    '''
    fnames = os.listdir(inputdir)
    fnames_dic = {}
    for f in fnames:
        if f.endswith(ext):
            # sample.bw -> sample
            sample = f.split(".")[0]
            # handle sample if contains { or }
            for badchar in ["{", "}", ","]:
                sample = sample.replace(badchar, "")
            if sample not in fnames_dic:
                fnames_dic[sample] = []
            # make new fname
            newf = ''.join([sample, ext])
            fnames_dic[sample].append(os.path.join(inputdir, newf))
    return fnames_dic

def get_geno_from_samp(samp):
    samp = os.path.splitext(samp)[0]
    return(samp.split("_")[1])

def get_assay_from_samp(samp):
    samp = os.path.splitext(samp)[0]
    return(samp.split("_")[0])

def get_winsize_from_samp(samp, prefix="win"):
    samp = os.path.splitext(samp)[0]
    winsize = samp.split("_")[2]
    winsize_prefix = ''.join([prefix, winsize])
    return(winsize_prefix)

def main():
    parser = argparse.ArgumentParser(description='Make trackhubs for UCSC browser using bigBed files. \
                                     Outputs to CURRENT DIRECTORY.')
    parser.add_argument('inputdir', metavar='INDIR',
                        help='Directory containing bigBed files .bb ending')
    parser.add_argument('--quiet', '-q', action='store_true',
                         help='Suppress some print statements')
    parser.add_argument('--render', '-r', action='store_true',
                         help='Render file to current dir')
    parser.add_argument('--upload', '-u', action='store_true',
                         help='Upload file to webserver')
    args = parser.parse_args()

    # store command line arguments for reproducibility
    CMD_INPUTS = ' '.join(['python'] + sys.argv)    # easy printing later
    # store argparse inputs for reproducibility / debugging purposes
    args_dic = vars(args)
    ARG_INPUTS = ['%s=%s' % (key, val) for key, val in args_dic.iteritems()]
    ARG_INPUTS = ' '.join(ARG_INPUTS)

    # Print arguments supplied by user
    if not args.quiet:
        print('Command line inputs:')
        print(CMD_INPUTS)
        print ('Argparse variables:')
        print(ARG_INPUTS)

    # define constants (hard coded)
    dirname = "cyclix_rhythmic_bins_genomewide_all_assays"
    hubname = "cyclix_rhythmic_bins_genomewide_all_assays"
    shortlab = "cyclixRhythBins"
    longlab = "Amplitude, phase, and pvalue of rhythmic marks in bins"
    email = "jake.yeung@epfl.ch"
    # url = "http://upnaepc2.epfl.ch"
    url = "http://upnaesrv1.epfl.ch"
    assay = "bigbed"
    jvis = "dense"
    # bigbed options loaded into ViewTrack
    jrgb = "on"
    # jspectrum = "on"
    scoremax = 1000
    scoremin = 0

    # define URLs
    url_main = "%s/%s" % (url, dirname)
    url_base = "%s/%s/data" % (url, dirname)
    # upload_main = "~/Sites/%s" % dirname
    # upload_base = "~/Sites/%s/data" % dirname
    upload_main = "/data/web/sites/%s" % dirname
    upload_base = "/data/web/sites/%s/data" % dirname
    ftype = "bigBed 9"
    host = "upnaesrv1.epfl.ch"
    user = "websync"

    # define constants
    genomebuild = "mm9"

    files_dic = get_files_from_dir(args.inputdir, ext=".bb")


    assay_dic = {}
    geno_dic = {}
    winsize_dic = {}
    for sample in files_dic.keys():
        # make trivial dics geno=geno, assay=assay ...
        assay_dic[get_assay_from_samp(sample)] = get_assay_from_samp(sample)
        geno_dic[get_geno_from_samp(sample)] = get_geno_from_samp(sample)
        winsize_dic[get_winsize_from_samp(sample)] = get_winsize_from_samp(sample)


    # init hub genomes file genome trackdb
    # Make my hub
    hub = Hub(hub = hubname,
              short_label = shortlab,
              long_label = longlab,
              email = email,
              url = "%s/%s" % (url, dirname))

    hub.url = os.path.join(url_main, "%s.hub.txt" % hub.hub)

    genomes_file = GenomesFile()
    genome = Genome(genomebuild)
    trackdb = TrackDb()

    # add remote fn
    # hub.remote_fn = os.path.join(upload_main, "hub.txt")
    # genomes_file.remote_fn = os.path.join(upload_main, "genomes.txt")
    hub.remote_fn = upload_main
    genomes_file.remote_fn = upload_main
    trackdb.remote_fn = os.path.join(upload_main, genomebuild, "trackDb.txt")

    hub.add_genomes_file(genomes_file)
    genome.add_trackdb(trackdb)
    genomes_file.add_genome(genome)

    # init composite
    composite = CompositeTrack(name = hubname,
                               short_label = shortlab,
                               long_label = longlab,
                               tracktype = ftype)
    # make subgroups
    subgroups = [
        SubGroupDefinition(name = "assay",
                           label = "assay",
                           mapping = assay_dic),
        SubGroupDefinition(name = "genotype",
                           label = "genotype",
                           mapping = geno_dic),
        SubGroupDefinition(name = "window_size",
                           label = "window_size",
                           mapping = winsize_dic),
    ]
    composite.add_subgroups(subgroups)
    # make viewTrack, a hierarchy containing my files, for example
    view = ViewTrack(name = "%sViewTrack" % assay,
                     view = "%s" % assay,
                     visibility = jvis,
                     tracktype = ftype,
                     short_label = "%s" % assay,
                     long_label = "%s assay" % assay,
                     # big bed labels
                     itemRgb = jrgb,
                     scoreMin = scoremin,
                     scoreMax = scoremax)
    composite.add_view(view)

    # make track
    for sample, wfs in files_dic.iteritems():
        for wf in wfs:
            sampname = os.path.basename(wf)
            assay = get_assay_from_samp(sampname)
            genotype = get_geno_from_samp(sampname)
            winsize = get_winsize_from_samp(sampname)
            bname = sampname.split(".")[0]
            track = Track(name = bname,
                            tracktype = ftype,
                            url = os.path.join(url_base, "%s" % sampname),
                            local_fn = os.path.abspath(wf),
                            remote_fn = os.path.join(upload_base, "%s" % sampname),
                            visibility = jvis,
                            shortLabel = bname,
                            longLabel = bname,
                            # spectrum = jspectrum,
                            itemRgb = jrgb,
                            scoreMin = scoremin,
                            scoreMax = scoremax,
                            subgroups = {"assay" : assay,
                                         "genotype" : genotype,
                                         "window_size" : winsize})
            view.add_tracks(track)
    trackdb.add_tracks(composite)

    print 'Track looks like this:'
    print trackdb

    if args.render:
        print 'Rendering to %s' % hub.local_fn
        results = hub.render()

    if args.upload:
        print 'Uploading to web@circadian.epfl.ch'
        for track in trackdb.tracks:
            upload_track(track = track, host = host, user = user)
        upload_hub(hub = hub, host = host, user = user)

    print 'Subgroups:'
    for sg in subgroups:
        print sg
    print 'Hub URL if you uploaded to server: %s' % hub.url

if __name__ == '__main__':
    main()
