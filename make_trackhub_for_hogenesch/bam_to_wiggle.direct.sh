#!/bin/sh
# Jake Yeung
# bam_to_wiggle.sh
# Converts BAM file to wiggle. 
# Uses bamToBed then genomeCoverageBed then bedGraphToBigWig.
# Make sure all bins are available in your path
# Requires genome size, which is downloaded from:
# http://hgdownload.soe.ucsc.edu/goldenPath/mm10/bigZips/mm10.chrom.sizes
# and placed in /scratch/el/monthly/jyeung/databases/mm10/mm10.chrom.sizes
# 2015-04-15

bamin=$1
wigout=$2
prefix=${bamin%.*}  # intermediate file
chrom_sizes=/scratch/el/monthly/jyeung/databases/mm10/mm10.chrom.sizes
[[ ! -e $bamin ]] && echo "BAM file does not exist: $1" && exit 1
# [[ -e $wigout ]] && echo "Outfile already exists. Not overwriting." && exit 1
# [[ -e $prefix.cov || -e $prefix.bed ]] && echo "Intermediate file exists. Not overwriting." && exit 1

echo "Make bedgraph directly"
bam2bedgraph $bamin > $prefix.bedgraphtemp
echo "Making bigWig file. $prefix.bedgraphtemp to $prefix.bw"
bedGraphToBigWig $prefix.bedgraphtemp $chrom_sizes $prefix.bw

# Remove .cov file if no problems
# echo "Removing intermediates $prefix.bed and $prefix.cov"
# rm $prefix.bed $prefix.cov

