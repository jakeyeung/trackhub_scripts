#!/bin/sh
# Jake Yeung
# bam_to_wiggle.sh
# Converts BAM file to wiggle. 
# Uses bamToBed then genomeCoverageBed then bedGraphToBigWig.
# Make sure all bins are available in your path
# Requires genome size, which is downloaded from:
# http://hgdownload.soe.ucsc.edu/goldenPath/mm10/bigZips/mm10.chrom.sizes
# and placed in /scratch/el/monthly/jyeung/databases/mm10/mm10.chrom.sizes
# 2015-04-15

bamin=$1
wigout=$2
prefix=${bamin%.*}  # intermediate file
chrom_sizes=/scratch/el/monthly/jyeung/databases/mm10/mm10.chrom.sizes
# chrom_sizes=/scratch/el/monthly/jyeung/databases/ensembl/ncbi_chr_lengths.txt
[[ ! -e $bamin ]] && echo "BAM file does not exist: $1" && exit 1
# [[ -e $prefix.cov || -e $prefix.bed ]] && echo "Intermediate file exists. Not overwriting." && exit 1


# Ran flagstat separately, so this is commented out and assumes $prefix.flagstat exists.
echo "Calculating number of reads of $bamin"
samtools flagstat $bamin > $prefix.flagstat

[[ ! -e $prefix.flagstat ]] && echo "$prefix.flagstat does not exist. Exiting" && exit 1
nreads=`awk 'NR==1{print $1}' $prefix.flagstat`
echo "nreads=$nreads"
scalefactor=`echo 1000000 $nreads | awk '{ print $1/$2 }'`
echo "Scale factor: $scalefactor"

# echo "Running: genomeCoverageBed -split -scale $scalefactor -ibam $bamin -bga -g $chrom_sizes > $prefix.cov"
genomeCoverageBed -bga -split -scale $scalefactor -ibam $bamin -g $chrom_sizes > $prefix.cov
# genomeCoverageBed -scale $scalefactor -ibam $bamin -bga -g $chrom_sizes > $prefix.cov
echo "Running: bedGraphToBigWig $prefix.cov $chrom_sizes $wigout"
bedGraphToBigWig $prefix.cov $chrom_sizes $wigout

# Remove .cov file if no problems
# echo "Removing intermediates $prefix.bed and $prefix.cov"
# rm $prefix.bed $prefix.cov

