#!/bin/sh
# Jake Yeung
# bed_to_wiggle.sh
# Converts BED file to wiggle. 
# Uses bamToBed then genomeCoverageBed then bedGraphToBigWig.
# Make sure all bins are available in your path
# Requires genome size, which is downloaded from:
# http://hgdownload.soe.ucsc.edu/goldenPath/mm10/bigZips/mm10.chrom.sizes
# and placed in /scratch/el/monthly/jyeung/databases/mm10/mm10.chrom.sizes
# 2015-04-15

bedin=$1
wigout=$2
prefix=${bedin%.*}  # intermediate file
chrom_sizes=/scratch/el/monthly/jyeung/databases/mm10/mm10.chrom.sizes
[[ ! -e $bedin ]] && echo "BAM file does not exist: $1" && exit 1
[[ ! -e $chrom_sizes ]] && echo "Error: $chrom_sizes does not exist." && exit 1

# no scalefactor, we assume beds are normalized
echo "Running: genomeCoverageBed -bga -i $bedin -g $chrom_sizes > $prefix.cov"
genomeCoverageBed -bga -i $bedin -g $chrom_sizes > $prefix.cov
# genomeCoverageBed -scale $scalefactor -ibam $bedin -bga -g $chrom_sizes > $prefix.cov
echo "Running: bedGraphToBigWig $prefix.cov $chrom_sizes $wigout"
bedGraphToBigWig $prefix.cov $chrom_sizes $wigout

