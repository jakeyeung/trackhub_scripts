#!/usr/bin/env python
'''
DESCRIPTION

    Make trackhubs for UCSC browser using bigWig files.
    Tutorials for making trackhubs with trackhub python package from:
    https://pythonhosted.org/trackhub/tutorial.html#creating-a-composite-track

FOR HELP

    python make_trackhubs.py --help

AUTHOR:      Jake Yeung (jake.yeung@epfl.ch)
LAB:         Computational Systems Biology Lab (http://naef-lab.epfl.ch)
CREATED ON:  2015-04-15
LAST CHANGE: see git log
LICENSE:     MIT License (see: http://opensource.org/licenses/MIT)

EXAMPLE

python make_trackhubs.py /scratch/el/monthly/jyeung/hogenesch_rna_seq/big_wigs /scratch/el/monthly/jyeung/hogenesch_rna_seq/big_wigs/trackDb.txt

'''

import sys, argparse, os
from trackhub import Hub, GenomesFile, Genome, TrackDb, Track
from trackhub import CompositeTrack
from trackhub.track import SubGroupDefinition
from trackhub import ViewTrack

from trackhub.upload import upload_track, upload_hub

# import numpy as np

def get_tissue_from_filename(fname):
    return fname.split('_')[0]

def get_time_from_filename(fname, prefix=None):
    time = fname.split('_')[1][2:4]
    # check time is an integer
    try:
        time = int(time)
    except ValueError:
        print 'Warning: expected time "%s" to be integer' % time
        return None
    if prefix:
        time = '%s%s' % (prefix, time)
    return time

def get_wig_files(indir):
    wigfiles = []
    # Get all wig files from directory
    for f in os.listdir(indir):
        if f.endswith(".bw"):
            wigfiles.append(f)
    return wigfiles

def extract_tissue_names(wigfiles):
    '''
    Expect tissue name of wigfile to be at beginning of wigfile, with '_' to denote end
    of tissue name
    '''
    tissues = []
    for wf in wigfiles:
        tissue = get_tissue_from_filename(wf)
        tissues.append(tissue)

    # return uniques only, also sort by alphabetical order
    return(sorted(list(set(tissues))))

def extract_times(wigfiles):
    '''
    Expect fileformat: WFat_CTxxAligned.sortedByCoordinate.out.bw
    Extract the xx from the filename
    '''
    times = []
    for wf in wigfiles:
        time = get_time_from_filename(wf)
        times.append(time)
    return(sorted(list(set(times))))

def make_tissues_times_dic(wigfiles):
    tissues = extract_tissue_names(wigfiles)
    times = extract_times(wigfiles)

    tissues_dic = {}
    for tissue in tissues:
        tissues_dic[tissue] = tissue
    times_dic = {}
    for time in times:
        times_dic['CT%s' % time] = 'CT%s'% time
    return tissues_dic, times_dic

def make_name_from_filename(fname):
    '''
    from WFat_CT34Aligned.sortedByCoord.out.bw get WFat_CT34
    '''
    tissue = get_tissue_from_filename(fname)
    time = get_time_from_filename(fname)
    new_name = '%s_CT%s' % (tissue, time)
    return new_name

def main():
    parser = argparse.ArgumentParser(description='Make trackhubs for UCSC browser using bigWig files. Outputs to CURRENT DIRECTORY.')
    parser.add_argument('inputdir', metavar='INDIR',
                        help='Directory containing bigWig files .bw ending')
    parser.add_argument('--quiet', '-q', action='store_true',
                         help='Suppress some print statements')
    parser.add_argument('--render', '-r', action='store_true',
                         help='Render file to current dir')
    parser.add_argument('--upload', '-u', action='store_true',
                         help='Upload file to webserver')
    parser.add_argument('--ymax', metavar='NUMBER', default = 500, type = float,
                        help='Ymax range for bigWig signal. Default 500')
    args = parser.parse_args()

    # store command line arguments for reproducibility
    CMD_INPUTS = ' '.join(['python'] + sys.argv)    # easy printing later
    # store argparse inputs for reproducibility / debugging purposes
    args_dic = vars(args)
    ARG_INPUTS = ['%s=%s' % (key, val) for key, val in args_dic.iteritems()]
    ARG_INPUTS = ' '.join(ARG_INPUTS)

    # Print arguments supplied by user
    if not args.quiet:
        print('Command line inputs:')
        print(CMD_INPUTS)
        print ('Argparse variables:')
        print(ARG_INPUTS)

    # define URLs
    url_main = "http://upnaepc2.epfl.ch/hogenesch_RNASeq"
    url_base = "http://upnaepc2.epfl.ch/hogenesch_RNASeq/data"
    upload_main = "~/Sites/hogenesch_RNAseq"
    upload_base = "~/Sites/hogenesch_RNAseq/data"
    bigwigtype = "bigWig 0 %s" % args.ymax
    host = "circadian.epfl.ch"
    user = "web"

    # define constants
    genomebuild = "mm10"

    wigfiles = get_wig_files(args.inputdir)

    tissues_dic, times_dic = make_tissues_times_dic(wigfiles)

    # init hub genomes file genome trackdb
    # Make my hub
    hub = Hub(hub = "hogenesch_rnaseq",
              short_label = "hogenesch",
              long_label = "Hogenesch 2014 PNAS paper RNA-Seq data",
              email = "jake.yeung@epfl.ch",
              url = "http://upnaepc2.epfl.ch/hogenesch_RNASeq/%s")

    hub.url = os.path.join(url_main, "%s.hub.txt" % hub.hub)

    genomes_file = GenomesFile()
    genome = Genome(genomebuild)
    trackdb = TrackDb()

    # add remote fn
    # hub.remote_fn = os.path.join(upload_main, "hub.txt")
    # genomes_file.remote_fn = os.path.join(upload_main, "genomes.txt")
    hub.remote_fn = upload_main
    genomes_file.remote_fn = upload_main
    trackdb.remote_fn = os.path.join(upload_main, genomebuild, "trackDb.txt")

    hub.add_genomes_file(genomes_file)
    genome.add_trackdb(trackdb)
    genomes_file.add_genome(genome)

    # init composite
    composite = CompositeTrack(name = "hogenesch_rnaseq",
                               short_label = "hogenesch",
                               long_label = "Hogenesch 2014 PNAS paper RNA-Seq data",
                               tracktype = "bigWig")
    # make subgroups
    subgroups = [

        SubGroupDefinition(name = "tissue",
                           label = "Tissue",
                           mapping = tissues_dic),
        SubGroupDefinition(name = "time",
                           label = "TimeCT",
                           mapping = times_dic)
    ]
    composite.add_subgroups(subgroups)
    # make viewTrack, a hierarchy containing my wig files, for example
    rnaseq_view = ViewTrack(name = "rnaseqViewTrack",
                            view = "RnaSEQ",
                            visibility = "full",
                            tracktype = bigwigtype,
                            short_label = "RNASeq",
                            long_label = "RNA-Seq Signal")
    composite.add_view(rnaseq_view)

    # make track
    for wf in sorted(wigfiles):
        tissue = get_tissue_from_filename(wf)
        time = get_time_from_filename(wf, prefix="CT")
        if time == "CT22":
            jvisibility = "full"
        else:
            jvisibility = "hide"
        sampname = make_name_from_filename(wf)
        basename = os.path.basename(wf)
        track = Track(name = sampname,
                      tracktype = bigwigtype,
                      url = os.path.join(url_base, '%s.bw' % sampname),
                      local_fn = os.path.abspath(os.path.join(args.inputdir, wf)),
                      remote_fn = os.path.join(upload_base, '%s.bw' % sampname),
                      visibility = jvisibility,
                      shortLabel = sampname,
                      longLabel = sampname,
                      subgroups = {"tissue" : tissue,
                                   "time" : time})
        rnaseq_view.add_tracks(track)
    trackdb.add_tracks(composite)

    print 'Track looks like this:'
    print trackdb

    if args.render:
        print 'Rendering to %s' % hub.local_fn
        results = hub.render()

    if args.upload:
        print 'Uploading to web@circadian.epfl.ch'
        for track in trackdb.tracks:
            upload_track(track = track, host = host, user = user)
        upload_hub(hub = hub, host = host, user = user)

    print 'Subgroups:'
    for sg in subgroups:
        print sg
    print 'Hub URL if you uploaded to server: %s' % hub.url

if __name__ == '__main__':
    main()
