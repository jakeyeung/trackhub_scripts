#!/bin/sh
# Jake Yeung
# run_bam_to_wiggle.sh
# Run in batch: bam_to_wiggle.sh
# 2015-04-15

convertscript="/Home/jyeung/projects/make_trackhub_for_hogenesch/bam_to_wiggle.sh"
bamdir="/scratch/el/monthly/jyeung/hogenesch_rna_seq/big_wigs_normalized"
outdir="/scratch/el/monthly/jyeung/hogenesch_rna_seq/big_wigs_normalized"
nohupout="/scratch/el/monthly/jyeung/nohups/bamToWiggle_flagstat_scaled"
[[ ! -d $nohupout ]] && mkdir $nohupout

for d in `ls -d $bamdir/*.bam`
do
	bname=$(basename $d)
	bsub -M 10194304 -o $nohupout/$bname.out -e $nohupout/$bname.err "bash $convertscript $d ${d%.*}.scaled.bw"
done
