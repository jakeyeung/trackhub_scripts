#!/usr/bin/env python
'''
DESCRIPTION

    Make trackhubs for UCSC browser using bigWig files.
    Tutorials for making trackhubs with trackhub python package from:
    https://pythonhosted.org/trackhub/tutorial.html#creating-a-composite-track

    Jeromes lane 1 4C-Seq samples

FOR HELP

    python make_trackhubs.py --help

AUTHOR:      Jake Yeung (jake.yeung@epfl.ch)
LAB:         Computational Systems Biology Lab (http://naef-lab.epfl.ch)
CREATED ON:  2015-10-24
LAST CHANGE: see git log
LICENSE:     MIT License (see: http://opensource.org/licenses/MIT)

EXAMPLE

'''

import sys, argparse, os
from trackhub import Hub, GenomesFile, Genome, TrackDb, Track
from trackhub import CompositeTrack
from trackhub.track import SubGroupDefinition
from trackhub import ViewTrack

from trackhub.upload import upload_track, upload_hub

# import numpy as np

def get_wig_files_from_directory(inputdir):
    '''
    Get all *.bw files (e.g. name.bw)
    Output as dic with {name: name.bw}
    '''
    fnames = os.listdir(inputdir)
    fnames_dic = {}
    for f in fnames:
        if f.endswith(".bw"):
            # sample.bw -> sample
            sample = f.split(".")[0]
            if sample not in fnames_dic:
                fnames_dic[sample] = []
            fnames_dic[sample].append(os.path.join(inputdir, f))
    return fnames_dic

def main():
    parser = argparse.ArgumentParser(description='Make trackhubs for UCSC browser using bigWig files. Outputs to CURRENT DIRECTORY.')
    parser.add_argument('inputdir', metavar='INDIR',
                        help='Directory containing bigWig files .bw ending')
    parser.add_argument('--quiet', '-q', action='store_true',
                         help='Suppress some print statements')
    parser.add_argument('--render', '-r', action='store_true',
                         help='Render file to current dir')
    parser.add_argument('--upload', '-u', action='store_true',
                         help='Upload file to webserver')
    parser.add_argument('--ymax', metavar='NUMBER', default = 1, type = float,
                        help='Ymax range for bigWig signal. Default 1')
    args = parser.parse_args()

    # store command line arguments for reproducibility
    CMD_INPUTS = ' '.join(['python'] + sys.argv)    # easy printing later
    # store argparse inputs for reproducibility / debugging purposes
    args_dic = vars(args)
    ARG_INPUTS = ['%s=%s' % (key, val) for key, val in args_dic.iteritems()]
    ARG_INPUTS = ' '.join(ARG_INPUTS)

    # Print arguments supplied by user
    if not args.quiet:
        print('Command line inputs:')
        print(CMD_INPUTS)
        print ('Argparse variables:')
        print(ARG_INPUTS)

    # define constants (hard coded)
    dirname = "Jerome4CSeq20151024"
    hubname = "Jerome4CSeq20151024"
    shortlab = "Naef"
    longlab = "Lane 1 sequencing reads"
    email = "jake.yeung@epfl.ch"
    # url = "http://upnaepc2.epfl.ch"
    url = "http://upnaesrv1.epfl.ch"
    assay = "4cseq"

    # define URLs
    url_main = "%s/%s" % (url, dirname)
    url_base = "%s/%s/data" % (url, dirname)
    # upload_main = "~/Sites/%s" % dirname
    # upload_base = "~/Sites/%s/data" % dirname
    upload_main = "/data/web/sites/%s" % dirname
    upload_base = "/data/web/sites/%s/data" % dirname
    bigwigtype = "bigWig 0 %s" % args.ymax
    # host = "circadian.epfl.ch"
    # user = "web"
    host = "upnaesrv1.epfl.ch"
    user = "websync"

    # define constants
    genomebuild = "mm10"

    wigfiles_dic = get_wig_files_from_directory(args.inputdir)

    samples_dic = {}
    for sample in wigfiles_dic.keys():
        samples_dic[sample] = sample

    # init hub genomes file genome trackdb
    # Make my hub
    hub = Hub(hub = hubname,
              short_label = shortlab,
              long_label = longlab,
              email = email,
              url = "%s/%s" % (url, dirname))

    hub.url = os.path.join(url_main, "%s.hub.txt" % hub.hub)

    genomes_file = GenomesFile()
    genome = Genome(genomebuild)
    trackdb = TrackDb()

    # add remote fn
    # hub.remote_fn = os.path.join(upload_main, "hub.txt")
    # genomes_file.remote_fn = os.path.join(upload_main, "genomes.txt")
    hub.remote_fn = upload_main
    genomes_file.remote_fn = upload_main
    trackdb.remote_fn = os.path.join(upload_main, genomebuild, "trackDb.txt")

    hub.add_genomes_file(genomes_file)
    genome.add_trackdb(trackdb)
    genomes_file.add_genome(genome)

    # init composite
    composite = CompositeTrack(name = hubname,
                               short_label = shortlab,
                               long_label = longlab,
                               tracktype = "bigWig")
    # make subgroups
    subgroups = [

        SubGroupDefinition(name = "sample",
                           label = "sample",
                           mapping = samples_dic),
    ]
    composite.add_subgroups(subgroups)
    # make viewTrack, a hierarchy containing my wig files, for example
    view = ViewTrack(name = "%sViewTrack" % assay,
                            view = "%s" % assay,
                            visibility = "full",
                            tracktype = bigwigtype,
                            short_label = "%s" % assay,
                            long_label = "%s assay" % assay)
    composite.add_view(view)

    # make track
    for sample, wfs in wigfiles_dic.iteritems():
        for wf in wfs:
            sampname = os.path.basename(wf)
            bname = sampname.split(".")[0]
            track = Track(name = bname,
                            tracktype = bigwigtype,
                            url = os.path.join(url_base, "%s" % sampname),
                            local_fn = os.path.abspath(wf),
                            remote_fn = os.path.join(upload_base, "%s" % sampname),
                            visibility = "full",
                            shortLabel = bname,
                            longLabel = bname,
                            subgroups = {"sample" : sample})
            view.add_tracks(track)
    trackdb.add_tracks(composite)

    print 'Track looks like this:'
    print trackdb

    if args.render:
        print 'Rendering to %s' % hub.local_fn
        results = hub.render()

    if args.upload:
        print 'Uploading to web@circadian.epfl.ch'
        for track in trackdb.tracks:
            upload_track(track = track, host = host, user = user)
        upload_hub(hub = hub, host = host, user = user)

    print 'Subgroups:'
    for sg in subgroups:
        print sg
    print 'Hub URL if you uploaded to server: %s' % hub.url

if __name__ == '__main__':
    main()
