#!/usr/bin/env python
'''
DESCRIPTION

    Make trackhubs for UCSC browser using bigWig files.
    Tutorials for making trackhubs with trackhub python package from:
    https://pythonhosted.org/trackhub/tutorial.html#creating-a-composite-track

FOR HELP

    python make_trackhubs.py --help

AUTHOR:      Jake Yeung (jake.yeung@epfl.ch)
LAB:         Computational Systems Biology Lab (http://naef-lab.epfl.ch)
CREATED ON:  2015-04-15
LAST CHANGE: see git log
LICENSE:     MIT License (see: http://opensource.org/licenses/MIT)

EXAMPLE

'''

import sys, argparse, os
from trackhub import Hub, GenomesFile, Genome, TrackDb, Track
from trackhub import CompositeTrack
from trackhub.track import SubGroupDefinition
from trackhub import ViewTrack

from trackhub.upload import upload_track, upload_hub

# import numpy as np

def get_wig_files(inputdir, suffix=".bw"):
    '''
    Get all *.bw files from directory.
    Output as a dictionary with {samp: filename}
    '''
    fnames = os.listdir(inputdir)
    fnames_dic = {}
    for f in fnames:
        if f.endswith(suffix):
            # extract samp name from filename
            samp = f.split(".")[0]
            fnames_dic[samp] = os.path.join(inputdir, f)
    return fnames_dic

def main():
    parser = argparse.ArgumentParser(description='Make trackhubs for UCSC browser using bigWig files. Outputs to CURRENT DIRECTORY.')
    parser.add_argument('inputdir', metavar='INDIR',
                        help='Directory containing bigWig files normalized.bw ending')  # to distinguish between .bw from hts station
    parser.add_argument('--quiet', '-q', action='store_true',
                         help='Suppress some print statements')
    parser.add_argument('--render', '-r', action='store_true',
                         help='Render file to current dir')
    parser.add_argument('--upload', '-u', action='store_true',
                         help='Upload file to webserver')
    args = parser.parse_args()

    # store command line arguments for reproducibility
    CMD_INPUTS = ' '.join(['python'] + sys.argv)    # easy printing later
    # store argparse inputs for reproducibility / debugging purposes
    args_dic = vars(args)
    ARG_INPUTS = ['%s=%s' % (key, val) for key, val in args_dic.iteritems()]
    ARG_INPUTS = ' '.join(ARG_INPUTS)

    # Print arguments supplied by user
    if not args.quiet:
        print('Command line inputs:')
        print(CMD_INPUTS)
        print ('Argparse variables:')
        print(ARG_INPUTS)

    # define constants (hard coded)
    dirname = "MaleFemale_DHS_mm10"
    hubname = "MaleFemale_DHS_mm10"
    shorthublab = "MaleFemale_DHS"
    longhublab = shorthublab
    trackname1 = "Male_Female_MaleGH"
    trackname2 = "Male_Female_Log2Ratio"
    trackname_lst = [trackname1, trackname2]
    shortlab1 = "MaleFemale_DHS_mm10"
    shortlab2 = "MaleFemale_Log2Ratio_mm10"
    shortlab_lst = [shortlab1, shortlab2]
    longlab1 = "Replicates merged Waxman Male Female Dnase-I hypersensitivity assays mm10"
    longlab2 = "Male Female Log2Ratio Dnase-I hypersensitivity assays mm10"
    longlab_lst = [longlab1, longlab2]
    bigwigtype1 = "bigWig 0 150"
    bigwigtype2 = "bigWig -5 5"
    bigwigtype_lst = [bigwigtype1, bigwigtype2]
    email = "jake.yeung@epfl.ch"
    # url = "http://upnaepc2.epfl.ch"
    url = "http://upnaesrv1.epfl.ch"
    assay = "DHS"

    # define URLs
    url_main = "%s/%s" % (url, dirname)
    url_base = "%s/%s/data" % (url, dirname)
    # upload_main = "~/Sites/%s" % dirname
    # upload_base = "~/Sites/%s/data" % dirname
    upload_main = "/data/web/sites/%s" % dirname
    upload_base = "/data/web/sites/%s/data" % dirname
    # host = "circadian.epfl.ch"
    # user = "web"
    host = "upnaesrv1.epfl.ch"
    user = "websync"

    # define constants
    genomebuild = "mm10"

    wigfiles_dic1 = get_wig_files(args.inputdir, suffix="unique.normalized.bw")
    wigfiles_dic2 = get_wig_files(args.inputdir, suffix="Log2Ratio.bw")

    wigfiles_dic_lst = [wigfiles_dic1, wigfiles_dic2]

    samps_dic1 = {}
    for samp in wigfiles_dic1.keys():
        samps_dic1[samp] = samp

    samps_dic2 = {}
    for samp in wigfiles_dic2.keys():
        samps_dic2[samp] = samp

    samps_dic_lst = [samps_dic1, samps_dic2]

    # init hub genomes file genome trackdb
    # Make my hub
    hub = Hub(hub = hubname,
            short_label = shorthublab,
            long_label = longhublab,
            email = email,
            url = "%s/%s" % (url, dirname))

    hub.url = os.path.join(url_main, "%s.hub.txt" % hub.hub)

    genomes_file = GenomesFile()
    genome = Genome(genomebuild)
    trackdb = TrackDb()
    hub.remote_fn = upload_main
    genomes_file.remote_fn = upload_main
    trackdb.remote_fn = os.path.join(upload_main, genomebuild, "trackDb.txt")

    hub.add_genomes_file(genomes_file)
    genome.add_trackdb(trackdb)
    genomes_file.add_genome(genome)

    for trackname, shortlab, longlab, bigwigtype, samps_dic, wigfiles_dic in \
            zip(trackname_lst, shortlab_lst, longlab_lst,
                bigwigtype_lst, samps_dic_lst, wigfiles_dic_lst):
        print shortlab

        print(samps_dic)

        # init composite
        composite = CompositeTrack(name = trackname,
                                short_label = shortlab,
                                long_label = longlab,
                                tracktype = "bigWig")
        # make subgroups
        subgroups = [

            SubGroupDefinition(name = "samp",
                            label = "Samp",
                            mapping = samps_dic),
        ]
        composite.add_subgroups(subgroups)
        # make viewTrack, a hierarchy containing my wig files, for example
        view = ViewTrack(name = "%sViewTrack" % trackname,
                                view = "%s" % shortlab,
                                visibility = "full",
                                tracktype = bigwigtype,
                                short_label = "%s" % shortlab,
                                long_label = "%s" % longlab)
        composite.add_view(view)

        # make track
        for samp, wf in wigfiles_dic.iteritems():
            sampname = os.path.basename(wf)
            bname = sampname.split(".")[0]
            track = Track(name = bname,
                            tracktype = bigwigtype,
                            url = os.path.join(url_base, "%s" % sampname),
                            local_fn = os.path.abspath(wf),
                            remote_fn = os.path.join(upload_base, "%s" % sampname),
                            visibility = "full",
                            shortLabel = bname,
                            longLabel = bname,
                            subgroups = {"samp" : samp})
            view.add_tracks(track)
        trackdb.add_tracks(composite)
        print 'Track looks like this:'
        print trackdb

    if args.render:
        print 'Rendering to %s' % hub.local_fn
        results = hub.render()

    if args.upload:
        print 'Uploading to web@circadian.epfl.ch'
        for track in trackdb.tracks:
            upload_track(track = track, host = host, user = user)
        upload_hub(hub = hub, host = host, user = user)

    print 'Subgroups:'
    for sg in subgroups:
        print sg
    print 'Hub URL if you uploaded to server: %s' % hub.url

if __name__ == '__main__':
    main()
