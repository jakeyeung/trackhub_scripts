#!/bin/sh
# Jake Yeung
# run.make_trackhubs.sh
# Run trackhubs for male female dhs  
# 2016-12-18

makescript="/Home/jyeung/projects/make_trackhub_scripts/make_trackhub_for_male_female_dhs/make_trackhubs.py"
indir="/archive/epfl/upnae/jyeung/sex-specificity/from_hts/processed_bw"

[[ ! -e $makescript ]] && echo "$makescript not found, exiting" && exit 1
[[ ! -d $indir ]] && echo "$indir not found, exiting" && exit 1

python $makescript $indir --render --upload
