#!/bin/sh
# Jake Yeung
# liftover_mm10_to_mm9.sh
# Liftover mm10 to mm9 
# 2016-09-24

mapchain="/scratch/el/monthly/jyeung/databases/liftover_files/mm10ToMm9.over.chain.gz"
tmpdir=""

# # Old has bugs because of NNNs and capital letters 
# indir="/scratch/el/monthly/jyeung/motevo_dhs_outputs/motevo_outputs/closestbed_renamed"
# outdir="/scratch/el/monthly/jyeung/motevo_dhs_outputs/motevo_outputs/closestbed_renamed_mm9"

# New cleaned version 2017-07-02
indir="/scratch/el/monthly/jyeung/motevo_dhs_outputs/motevo_outputs_cleaned/bed_stranded_renamed"
outdir="/scratch/el/monthly/jyeung/motevo_dhs_outputs/motevo_outputs_cleaned/bed_stranded_renamed_mm9"

[[ ! -d $indir ]] && echo "$indir not found, exiting" && exit 1
[[ ! -d $outdir ]] && mkdir $outdir
# [[ ! -d $outdir ]] && echo "$outdir not found, exiting" && exit 1

n=0
maxjobs=20

for f in `ls -d $indir/*.bed`; do 
	base=$(basename $f)
	basenoext=${base%%.*}
	fout=$outdir/$basenoext.mm9.bed
	liftOver -bedPlus=3 $f $mapchain $fout $outdir/$basenoext.mm9.unmapped&
	if (( $(($((++n)) % $maxjobs)) == 0 )) ; then
		# define maxjobs and n using maxjobsn skeleton
		wait # wait until all have finished (not optimal, but most times good enough)
		echo $n wait
	fi
done
wait
