#!/bin/sh
# Jake Yeung
# remove_braces_commas.sh
# Remove braces and commas : trackhubs dont like it 
# 2016-01-06

maindir="/scratch/el/monthly/jyeung/motevo_dhs_outputs/motevo_outputs/bigbeds"
outdir="/scratch/el/monthly/jyeung/motevo_dhs_outputs/motevo_outputs/bigbeds_renamed"

maindir="/scratch/el/monthly/jyeung/motevo_dhs_outputs/motevo_outputs/closestbed"
outdir="/scratch/el/monthly/jyeung/motevo_dhs_outputs/motevo_outputs/closestbed_renamed"

[[ ! -d $maindir ]] && echo "$maindir not found, exiting" && exit 1
[[ ! -d $outdir ]] && mkdir $outdir

for b in `ls -d $maindir/*.bb`; do
	base=$(basename $b)
	newbase=`echo $base | sed -e 's/[\{\}]//g' -e 's/[,]/or/g' -e 's/[-]/to/g'`
	ln -s $b $outdir/$newbase
done
