#!/bin/sh
# Jake Yeung
# run.make_trackhubs.mm9.sh
# Run make trackhubs for mm9 
# 2016-09-24

script="/Home/jyeung/projects/make_trackhub_scripts/make_trackhub_for_motevo_motifs/make_trackhubs.py"
indir="/scratch/el/monthly/jyeung/motevo_dhs_outputs/motevo_outputs/bigbeds_mm9"

[[ ! -e $script ]] && echo "$script not found, exiting" && exit 1
[[ ! -d $indir ]] && echo "$indir not found, exiting" && exit 1

python $script --mm9 $indir --render --upload
