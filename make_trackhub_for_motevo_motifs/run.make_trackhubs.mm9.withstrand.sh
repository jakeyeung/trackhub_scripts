#!/bin/sh
# Jake Yeung
# run.make_trackhubs.mm9.sh
# Run make trackhubs for mm9 
# 2017-07-02

script="/Home/jyeung/projects/make_trackhub_scripts/make_trackhub_for_motevo_motifs/make_trackhubs.py"
indir="/scratch/el/monthly/jyeung/motevo_dhs_outputs/motevo_outputs_cleaned/bigbeds_stranded_mm9"  # bugfix (Ns and capital letters) and stranded

[[ ! -e $script ]] && echo "$script not found, exiting" && exit 1
[[ ! -d $indir ]] && echo "$indir not found, exiting" && exit 1

python $script --mm9 $indir --render --upload --has_strand
