#!/bin/sh
# Jake Yeung
# run.make_trackhubs.sh
# Run trackhubs
# 2016-07-26

# indir="/archive/epfl/upnae/jyeung/sleep_deprivation/data_from_charlotte.atacseq/bigwigs_merged.atacseq"  # also possible
# assay="ATACSeq"

indir="/archive/epfl/upnae/jyeung/sleep_deprivation/data_from_charlotte/bigwigs_merged"
assay="RNASeq"

makescript="/Home/jyeung/projects/make_trackhub_scripts/make_trackhub_for_sleepdep_merged/make_trackhubs.py"

python $makescript $indir $assay --render --upload
