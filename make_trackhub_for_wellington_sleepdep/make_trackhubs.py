#!/usr/bin/env python
'''
DESCRIPTION

    Make trackhubs for UCSC browser using bigWig files.
    Tutorials for making trackhubs with trackhub python package from:
    https://pythonhosted.org/trackhub/tutorial.html#creating-a-composite-track

FOR HELP

    python make_trackhubs.py --help

AUTHOR:      Jake Yeung (jake.yeung@epfl.ch)
LAB:         Computational Systems Biology Lab (http://naef-lab.epfl.ch)
CREATED ON:  2015-04-15
LAST CHANGE: see git log
LICENSE:     MIT License (see: http://opensource.org/licenses/MIT)

EXAMPLE

'''

import sys, argparse, os
from trackhub import Hub, GenomesFile, Genome, TrackDb, Track
from trackhub import CompositeTrack
from trackhub.track import SubGroupDefinition
from trackhub import ViewTrack

from trackhub.upload import upload_track, upload_hub

import re

# import numpy as np

def get_time_from_filename(wf):
    '''
    ZT78_SD.bw -> ZT78
    '''
    samp = make_name_from_filename(wf)
    time = samp.split("_")[0]
    return(time)

def get_treat_from_filename(wf):
    '''
    ZT72_SD.bw -> SD
    '''
    samp = make_name_from_filename(wf)
    treat = samp.split("_")[1]
    return(treat)

def get_wig_files(indir):
    wigfiles = []
    # Get all wig files from directory
    for f in os.listdir(indir):
        if f.endswith(".bw"):
            wigfiles.append(f)
    return wigfiles

def extract_treats(wigfiles):
    treats = []
    for wf in wigfiles:
        treat = get_treat_from_filename(wf)
        treats.append(treat)

    # return uniques only, also sort by alphabetical order
    return(sorted(list(set(treats))))

def extract_times(wigfiles):
    '''
    ZT72_SD.bw -> ZT72
    '''
    times = []
    for wf in wigfiles:
        time = get_time_from_filename(wf)
        times.append(time)
    return(sorted(list(set(times))))

def make_treats_times_dic(wigfiles):
    treats = extract_treats(wigfiles)
    times = extract_times(wigfiles)
    treats_dic = {}
    for treat in treats:
        treats_dic[treat] = treat
    times_dic = {}
    for time in times:
        times_dic['%s' % time] = '%s'% time
    return treats_dic, times_dic

def string_to_number(s):
    # from:
    # http://stackoverflow.com/questions/4528982/convert-alphabet-letters-to-number-in-python
    return(ord(s.lower()) - 96)

def make_name_from_filename(fname):
    '''
    Change ZT to T, and ZT0A to ZT01 ( A B C E to 1 2 3 5 )

    Otherwise just remove .bam  from fname
    '''
    new_name = os.path.splitext(fname)[0]
    return new_name

def main():
    parser = argparse.ArgumentParser(description='Make trackhubs for UCSC browser using bigWig files. Outputs to CURRENT DIRECTORY.')
    parser.add_argument('inputdir', metavar='INDIR',
                        help='Directory containing bigWig files .bw ending')
    parser.add_argument('--quiet', '-q', action='store_true',
                         help='Suppress some print statements')
    parser.add_argument('--render', '-r', action='store_true',
                         help='Render file to current dir')
    parser.add_argument('--upload', '-u', action='store_true',
                         help='Upload file to webserver')
    parser.add_argument('--ymax', metavar='NUMBER', default = 0, type = float,
                        help='Ymax range for bigWig signal. Default 0')
    parser.add_argument('--ymin', metavar='NUMBER', default = -50, type = float,
                        help='Ymin range for bigWig signal. Default 1')
    args = parser.parse_args()

    # store command line arguments for reproducibility
    CMD_INPUTS = ' '.join(['python'] + sys.argv)    # easy printing later
    # store argparse inputs for reproducibility / debugging purposes
    args_dic = vars(args)
    ARG_INPUTS = ['%s=%s' % (key, val) for key, val in args_dic.iteritems()]
    ARG_INPUTS = ' '.join(ARG_INPUTS)

    # Print arguments supplied by user
    if not args.quiet:
        print('Command line inputs:')
        print(CMD_INPUTS)
        print ('Argparse variables:')
        print(ARG_INPUTS)

    assay = "wellingtonFootprints"

    # define constants (hard coded)
    dirname = "sleep_deprivation_atacseq_%s" % assay
    hubname = dirname
    shortlab = dirname
    longlab = "Sleep deprivation for 6 hours time course in cortex. Assay %s" % assay
    email = "jake.yeung@epfl.ch"
    # url = "http://upnaepc2.epfl.ch"
    url = "http://upnaesrv1.epfl.ch"
    assay = "bigWig"
    jvis = "full"

    # define URLs
    url_main = "%s/%s" % (url, dirname)
    url_base = "%s/%s/data" % (url, dirname)
    # upload_main = "~/Sites/%s" % dirname
    # upload_base = "~/Sites/%s/data" % dirname
    upload_main = "/data/web/sites/%s" % dirname
    upload_base = "/data/web/sites/%s/data" % dirname
    ftype = "bigWig %s %s" % (args.ymin, args.ymax)
    # host = "circadian.epfl.ch"
    # user = "web"
    host = "upnaesrv1.epfl.ch"
    user = "websync"

    # define constants
    genomebuild = "mm10"


    wigfiles = get_wig_files(args.inputdir)

    treats_dic, times_dic = make_treats_times_dic(wigfiles)

    # init hub genomes file genome trackdb
    # Make my hub
    hub = Hub(hub = hubname,
              short_label = shortlab,
              long_label = longlab,
              email = email,
              url = "%s/%s" % (url, dirname))

    hub.url = os.path.join(url_main, "%s.hub.txt" % hub.hub)

    genomes_file = GenomesFile()
    genome = Genome(genomebuild)
    trackdb = TrackDb()

    # add remote fn
    # hub.remote_fn = os.path.join(upload_main, "hub.txt")
    # genomes_file.remote_fn = os.path.join(upload_main, "genomes.txt")
    hub.remote_fn = upload_main
    genomes_file.remote_fn = upload_main
    trackdb.remote_fn = os.path.join(upload_main, genomebuild, "trackDb.txt")

    hub.add_genomes_file(genomes_file)
    genome.add_trackdb(trackdb)
    genomes_file.add_genome(genome)

    # init composite
    composite = CompositeTrack(name = hubname,
                               short_label = shortlab,
                               long_label = longlab,
                               tracktype = ftype)
    # make subgroups
    subgroups = [

        SubGroupDefinition(name = "treat",
                           label = "Treatment",
                           mapping = treats_dic),
        SubGroupDefinition(name = "time",
                           label = "Time",
                           mapping = times_dic)
    ]
    composite.add_subgroups(subgroups)
    # make viewTrack, a hierarchy containing my wig files, for example
    track_view = ViewTrack(name = "wellingtonViewTrack",
                            view = assay,
                            visibility = "full",
                            tracktype = ftype,
                            short_label = assay,
                            long_label = assay)
    composite.add_view(track_view)

    # make track
    for wf in sorted(wigfiles):
        treat = get_treat_from_filename(wf)
        time = get_time_from_filename(wf)
        jvisibility = "full"
        sampname = make_name_from_filename(wf)
        basename = os.path.basename(wf)
        track = Track(name = sampname,
                      tracktype = ftype,
                      url = os.path.join(url_base, '%s.bw' % sampname),
                      local_fn = os.path.abspath(os.path.join(args.inputdir, wf)),
                      remote_fn = os.path.join(upload_base, '%s.bw' % sampname),
                      visibility = jvisibility,
                      shortLabel = sampname,
                      longLabel = sampname,
                      subgroups = {"treat" : treat,
                                   "time" : time})
        track_view.add_tracks(track)
    trackdb.add_tracks(composite)

    print 'Track looks like this:'
    print trackdb

    if args.render:
        print 'Rendering to %s' % hub.local_fn
        results = hub.render()

    if args.upload:
        print 'Uploading to web@circadian.epfl.ch'
        for track in trackdb.tracks:
            upload_track(track = track, host = host, user = user)
        upload_hub(hub = hub, host = host, user = user)

    print 'Subgroups:'
    for sg in subgroups:
        print sg
    print 'Hub URL if you uploaded to server: %s' % hub.url

if __name__ == '__main__':
    main()
