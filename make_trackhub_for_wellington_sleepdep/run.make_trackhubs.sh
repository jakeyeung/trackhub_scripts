#!/bin/sh
# Jake Yeung
# run.make_trackhubs.sh
#  
# 2017-02-18

pyscript="/Home/jyeung/projects/make_trackhub_scripts/make_trackhub_for_wellington_sleepdep/make_trackhubs.py"
indir="/archive/epfl/upnae/jyeung/sleep_deprivation/atacseq_analysis_from_hts/wellington_wigs"
[[ ! -e $pyscript ]] && echo "$pyscript not found, exiting" && exit 1
[[ ! -d $indir ]] && echo "$indir not found, exiting" && exit 1

python $pyscript --render --upload --ymin -50 --ymax 0 $indir
