#!/usr/bin/env python
'''
DESCRIPTION

    Make trackhubs for UCSC browser using bigWig files.
    Tutorials for making trackhubs with trackhub python package from:
    https://pythonhosted.org/trackhub/tutorial.html#creating-a-composite-track

FOR HELP

    python make_trackhubs.py --help

AUTHOR:      Jake Yeung (jake.yeung@epfl.ch)
LAB:         Computational Systems Biology Lab (http://naef-lab.epfl.ch)
CREATED ON:  2015-04-15
LAST CHANGE: see git log
LICENSE:     MIT License (see: http://opensource.org/licenses/MIT)

EXAMPLE

python make_trackhubs.py /scratch/el/monthly/jyeung/hogenesch_rna_seq/big_wigs /scratch/el/monthly/jyeung/hogenesch_rna_seq/big_wigs/trackDb.txt

'''

import sys, argparse, os
from trackhub import Hub, GenomesFile, Genome, TrackDb, Track
from trackhub import CompositeTrack
from trackhub import AggregateTrack
from trackhub.track import SubGroupDefinition
from trackhub import ViewTrack
from trackhub.upload import upload_track, upload_hub

# import numpy as np

def get_group_from_filename(fname):
    '''
    group-1_tiss-Kidney_geno-WT_time-ZT08.bw
    Get group1
    '''
    fname = os.path.splitext(fname)[0]
    groupstr = fname.split('_')[0]
    group = ''.join(groupstr.split("-"))
    return(group)

def get_geno_from_filename(fname):
    '''
    group-1_tiss-Kidney_geno-WT_time-ZT08.bw
    Get WT
    '''
    fname = os.path.splitext(fname)[0]
    genostr = fname.split('_')[2]
    geno = genostr.split('-')[1]
    return(geno)

def get_tissue_from_filename(fname):
    '''
    group-1_tiss-Kidney_geno-WT_time-ZT08.bw
    Get Kidney
    If you compare between tissues, you wont have tiss, return None
    '''
    fname = os.path.splitext(fname)[0]
    try:
        tissstr = fname.split('_')[1]
        tiss = tissstr.split('-')[1]
        return(tiss)
    except IndexError:
        return(None)

def get_time_from_filename(fname):
    '''
    group-1_tiss-Kidney_geno-WT_time-ZT08.bw
    Get ZT08
    When you compare between time, you wont have a time, so return None
    '''
    fname = os.path.splitext(fname)[0]
    try:
        timestr = fname.split('_')[3]  # time-ZT08
        time = timestr.split('-')[1]  # ZT08
        return(time)
    except IndexError:
        return(None)

def get_wig_files(indir):
    wigfiles = []
    # Get all wig files from directory
    for f in os.listdir(indir):
        if f.endswith(".bw"):
            wigfiles.append(f)
    return wigfiles

def extract_tissues(wigfiles):
    tissues = []
    for wf in wigfiles:
        tissue = get_tissue_from_filename(wf)
        tissues.append(tissue)
    # return uniques only, also sort by alphabetical order
    return(sorted(list(set(tissues))))

def extract_times(wigfiles):
    times = []
    for wf in wigfiles:
        time = get_time_from_filename(wf)
        times.append(time)
    return(sorted(list(set(times))))

def extract_genos(wigfiles):
    genos = []
    for wf in wigfiles:
        geno = get_geno_from_filename(wf)
        genos.append(geno)
    return(sorted(list(set(genos))))

def extract_groups(wigfiles):
    groups = []
    for wf in wigfiles:
        group = get_group_from_filename(wf)
        groups.append(group)
    return(sorted(list(set(groups))))

def make_tissues_times_genos_groups_dic(wigfiles):
    tissues = extract_tissues(wigfiles)
    times = extract_times(wigfiles)
    genos = extract_genos(wigfiles)
    groups = extract_groups(wigfiles)

    tissues_dic = {}
    for tissue in tissues:
        tissues_dic[tissue] = tissue
    times_dic = {}
    for time in times:
        times_dic['%s' % time] = '%s'% time
    genos_dic = {}
    for geno in genos:
        genos_dic[geno] = geno
    groups_dic = {}
    for group in groups:
        groups_dic[group] = group
    return tissues_dic, times_dic, genos_dic, groups_dic

def make_name_from_filename(fname, hubname):
    '''
    from group-1_tiss-Kidney_geno-WT_time-ZT08.bw get Group1_Kidney_WT_ZT08.bw
    Append hubname to end of sampname
    '''
    tissue = get_tissue_from_filename(fname)
    time = get_time_from_filename(fname)
    geno = get_geno_from_filename(fname)
    group = get_group_from_filename(fname)
    new_name = '%s_%s_%s_%s_%s' % (tissue, time, geno, group, hubname)
    # convert periods to underscores
    return new_name.replace(".", "_")

def time_to_color(time):
    # convert time to color, ZT08 and ZT20 only

    # Not rotated
    # col_zt20="255,0,255"
    # col_zt08="0,255,0"

    # rotated
    col_zt20="0,255,255"  # rotated
    col_zt08="255,0,0"  # rotated
    if time == "ZT08":
        return col_zt08
    elif time == "ZT20":
        return col_zt20
    else:
        print("Only ZT08 and ZT20 coded")
        return None

def add_aggregate(trackdb, inputdir, hubname, bigwigtype, url_base, upload_base, jlimits, autoscale, jaggregate, suffix=""):
    for group in ["group1", "group2"]:
        wigfiles = get_wig_files(os.path.join(inputdir, group))
        _, times_dic, _, _ = make_tissues_times_genos_groups_dic(wigfiles)
        for g in ['_'.join(["Liver-WT", group]),
                  '_'.join(["Liver-KO", group]),
                  '_'.join(["Kidney-WT", group])]:
            if group is "group1":
                jvisibility = "full"
            else:
                jvisibility = "hide"
            # init aggregate
            aggregate = AggregateTrack(name = '_'.join([g, suffix]),
                                    short_label = '_'.join([g, suffix]),
                                    long_label = '_'.join([g, suffix]),
                                    tracktype = "bigWig",
                                    aggregate = jaggregate,
                                    showSubtrackColorOnUi='on',
                                    # graphType = "points",
                                    viewLimits = jlimits,
                                    autoScale = autoscale,
                                    visibility = jvisibility)
            # make viewTrack, a hierarchy containing my wig files, for example
            # make track
            for wf in sorted(wigfiles):
                tissue = get_tissue_from_filename(wf)
                time = get_time_from_filename(wf)
                geno = get_geno_from_filename(wf)
                samplabel = '-'.join([tissue, geno])
                samplabel = '_'.join([samplabel, group])
                if samplabel != g:
                    # do only wigfiles that are matching to Tissue-Geno
                    continue
                sampname = make_name_from_filename(wf, hubname)
                basename = os.path.basename(wf)
                track = Track(name = sampname,
                            tracktype = bigwigtype,
                            url = os.path.join(url_base, '%s.bw' % sampname),
                            local_fn = os.path.abspath(os.path.join(inputdir, group, wf)),
                            remote_fn = os.path.join(upload_base, '%s.bw' % sampname),
                            shortLabel = sampname,
                            longLabel = sampname,
                            color = time_to_color(time))
                aggregate.add_subtrack(track)
            # add parameters
            trackdb.add_tracks(aggregate)
    return None

def main():
    parser = argparse.ArgumentParser(description='Make trackhubs for UCSC browser using bigWig files. Outputs to CURRENT DIRECTORY.')
    parser.add_argument('inputdir', metavar='INDIR',
                        help='Directory containing group1 and group, in which there are bigWig files .bw ending')
    parser.add_argument('hubname', metavar='HUBNAME', type = str,
                        help='Dirname which will be labelled and created')
    parser.add_argument('hubnameparent', metavar='HUBPARENT', type = str,
                        help='Parent directory to put trackhubs')
    parser.add_argument('--quiet', '-q', action='store_true',
                         help='Suppress some print statements')
    parser.add_argument('--render', '-r', action='store_true',
                         help='Render file to current dir')
    parser.add_argument('--upload', '-u', action='store_true',
                         help='Upload file to webserver')
    args = parser.parse_args()

    # store command line arguments for reproducibility
    CMD_INPUTS = ' '.join(['python'] + sys.argv)    # easy printing later
    # store argparse inputs for reproducibility / debugging purposes
    args_dic = vars(args)
    ARG_INPUTS = ['%s=%s' % (key, val) for key, val in args_dic.iteritems()]
    ARG_INPUTS = ' '.join(ARG_INPUTS)

    # Print arguments supplied by user
    if not args.quiet:
        print('Command line inputs:')
        print(CMD_INPUTS)
        print ('Argparse variables:')
        print(ARG_INPUTS)

    # define constants (hard coded)
    dirname = args.hubname
    srv_dir = os.path.join(args.hubnameparent, args.hubname)
    hubname = dirname
    shortlab = args.hubname
    longlab = "4CSeq %s" % args.hubname
    email = "jake.yeung@epfl.ch"
    # add main here
    url = "http://upnaesrv1.epfl.ch"
    assay = "4cseq"

    # define URLs
    url_main = "%s/%s" % (url, srv_dir)
    url_base = "%s/%s/data" % (url, srv_dir)
    # upload_main = "~/Sites/%s" % dirname
    # upload_base = "~/Sites/%s/data" % dirname
    upload_main = "/data/web/sites/%s" % srv_dir
    upload_base = "/data/web/sites/%s/data" % srv_dir
    bigwigtype = "bigWig"
    host = "upnaesrv1.epfl.ch"
    user = "websync"

    # define constants
    genomebuild = "mm9"
    jaggregate = "transparentOverlay"
    jlimits = "0:1.5"
    autoscale = "off"

    # init hub genomes file genome trackdb
    # Make my hub
    hub = Hub(hub = hubname,
              short_label = shortlab,
              long_label = longlab,
              email = email,
              url = "%s" % url_main)

    hub.url = os.path.join(url_main, "%s.hub.txt" % hub.hub)

    genomes_file = GenomesFile()
    genome = Genome(genomebuild)
    trackdb = TrackDb()

    # add remote fn
    # hub.remote_fn = os.path.join(upload_main, "hub.txt")
    # genomes_file.remote_fn = os.path.join(upload_main, "genomes.txt")
    hub.remote_fn = upload_main
    genomes_file.remote_fn = upload_main
    trackdb.remote_fn = os.path.join(upload_main, genomebuild, "trackDb.txt")

    hub.add_genomes_file(genomes_file)
    genome.add_trackdb(trackdb)
    genomes_file.add_genome(genome)

    # TODO potentially add colours somewhere here
    add_aggregate(trackdb, args.inputdir, hubname, bigwigtype, url_base, upload_base, jlimits, autoscale, jaggregate)

    print 'Track looks like this:'
    print trackdb

    if args.render:
        print 'Rendering to %s' % hub.local_fn
        results = hub.render()

    if args.upload:
        print 'Uploading to web@circadian.epfl.ch'
        for track in trackdb.tracks:
            upload_track(track = track, host = host, user = user)
        upload_hub(hub = hub, host = host, user = user)

    print 'Hub URL if you uploaded to server: %s' % hub.url

if __name__ == '__main__':
    main()
