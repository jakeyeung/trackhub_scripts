#!/usr/bin/env python
'''
DESCRIPTION

    Make trackhubs for UCSC browser using bigWig files.
    Tutorials for making trackhubs with trackhub python package from:
    https://pythonhosted.org/trackhub/tutorial.html#creating-a-composite-track

FOR HELP

    python make_trackhubs.py --help

AUTHOR:      Jake Yeung (jake.yeung@epfl.ch)
LAB:         Computational Systems Biology Lab (http://naef-lab.epfl.ch)
CREATED ON:  2017-11-15
LAST CHANGE: see git log
LICENSE:     MIT License (see: http://opensource.org/licenses/MIT)

EXAMPLE

python make_trackhubs.py /scratch/el/weekly/jyeung/groseq_liver/bigwigs GROSeqLiver GROSeqLiver

'''

import sys, argparse, os, re
from trackhub import Hub, GenomesFile, Genome, TrackDb, Track
from trackhub import CompositeTrack
from trackhub import AggregateTrack
from trackhub.track import SubGroupDefinition
from trackhub import ViewTrack
from trackhub.upload import upload_track, upload_hub

# import numpy as np

def get_wig_files(indir):
    wigfiles = []
    # Get all wig files from directory
    for f in os.listdir(indir):
        if f.endswith(".bw"):
            wigfiles.append(f)
    return wigfiles

def extract_tissues(wigfiles):
    tissues = []
    for wf in wigfiles:
        tissue = get_tissue_from_filename(wf)
        tissues.append(tissue)
    # return uniques only, also sort by alphabetical order
    return(sorted(list(set(tissues))))

def extract_times(wigfiles):
    times = []
    for wf in wigfiles:
        time = get_time_from_filename(wf)
        times.append(time)
    return(sorted(list(set(times))))

def extract_genos(wigfiles):
    genos = []
    for wf in wigfiles:
        geno = get_geno_from_filename(wf)
        genos.append(geno)
    return(sorted(list(set(genos))))

def make_name_from_filename(fname, hubname):
    '''
    output name: Geno-Time-hubname
    '''
    geno = get_geno_from_fname(fname)
    time = get_time_from_fname(fname)
    new_name = '%s_%s_%s' % (geno, time, hubname)
    # convert periods to underscores
    return new_name.replace(".", "_")

def time_to_color(time):
    # convert time to color, ZT08 and ZT20 only
    # Not rotated
    # col_zt20="255,0,255"
    # col_zt08="0,255,0"
    # rotated
    col_zt20="0,255,255"  # rotated  # blue
    col_zt08="255,0,0"  # rotated  # red
    if time == "ZT08":
        return col_zt08
    elif time == "ZT20":
        return col_zt20
    else:
        print("Only ZT08 and ZT20 coded, defaulting to blue")
        return("0,255,255")

def strand_to_color(strand):
    col_forward="0,255,255"  # blue
    col_reverse="255,0,0"  # red
    if strand == "forward":
        return col_forward
    elif strand == "reverse":
        return col_reverse
    else:
        print("Warning: strand must be forward and reverse, found: %s" %strand)
        return None


def make_tuple_fwd_reverse(wigfiles):
    '''
    Organize wigfiles into a list of tuples for forward and reverse
    '''
    wigfiles_tup_lst = []
    wigfiles_fwd = sorted([f for f in wigfiles if "forward" in f])
    for w_fwd in wigfiles_fwd:
        w_reverse = w_fwd.replace("forward", "reverse")
        wigfiles_tup_lst.append((w_fwd, w_reverse))
    return(wigfiles_tup_lst)

def make_subgroups(inputdir):
    '''
    Make subgroups to add to composite, doesnt work for aggregate unfortunately
    '''
    # init aggregate
    genos_dic = make_genos_dic(inputdir)
    times_dic = make_times_dic(inputdir)
    strands_dic = make_strands_dic(inputdir)
    # TODO potentially add colours somewhere here
    # make subgroups
    subgroups = [
        SubGroupDefinition(name = "geno",
                        label = "Genotype",
                        mapping = genos_dic),
        SubGroupDefinition(name = "time",
                        label = "TimeZT",
                        mapping = times_dic),
        SubGroupDefinition(name = "strand",
                        label = "Strand",
                        mapping = strands_dic)
    ]
    return(subgroups)

def add_aggregate(trackdb, inputdir, hubname, bigwigtype, url_base, upload_base, jlimits, autoscale, jaggregate, suffix=""):
    '''
    Make aggregate track for forward and reverse strand
    '''
    # jvisibility = "full"  # depends on geno
    wigfiles = get_wig_files(os.path.join(inputdir))
    wigfiles_tuple = make_tuple_fwd_reverse(wigfiles)
    # make viewTrack, a hierarchy containing my wig files, for example
    for wf_tup in sorted(wigfiles_tuple):
        # tuple: (fwd_wf, reverse_wf)
        geno = get_geno_from_fname(wf_tup[0])
        if geno == "WT":
            jvisibility = "full"
        else:
            jvisibility = "hide"
        time = get_time_from_fname(wf_tup[0])
        samplabel = '_'.join([geno, time])
        sampname = make_name_from_filename(wf_tup[0], hubname)  # 0 or 1 same name
        aggregate = AggregateTrack(name = '_'.join([samplabel, suffix]),
                                short_label = '_'.join([samplabel, suffix]),
                                long_label = '_'.join([samplabel, suffix]),
                                tracktype = "bigWig",
                                aggregate = jaggregate,
                                showSubtrackColorOnUi='on',
                                # graphType = "points",
                                # viewLimits = jlimits,
                                autoScale = autoscale,
                                visibility = jvisibility)
        # two tracks per aggregate: forward and reverse
        for wf in wf_tup:
            strand = get_strand_from_fname(wf)
            basename = os.path.basename(wf)
            sampname_full = '_'.join([sampname, strand])
            track = Track(name = sampname_full,
                          tracktype = bigwigtype,
                          url = os.path.join(url_base, '%s.bw' % sampname_full),
                          local_fn = os.path.abspath(os.path.join(inputdir, wf)),
                          remote_fn = os.path.join(upload_base, '%s.bw' % sampname_full),
                          shortLabel = sampname_full,
                          longLabel = sampname_full,
                          # viewLimits = "full",
                          # color = time_to_color(time))
                          color = strand_to_color(strand))
            aggregate.add_subtrack(track)
        trackdb.add_tracks(aggregate)

def get_geno_from_fname(inf):
    '''
    GRO-seq_WT_LiverZT16.bam.forward_SeqDepthNorm.bw -> WT
    and
    GRO-seq_Rev-erbA_WT_Liver_ZT10.bam.forward_SeqDepthNorm.bw -> Rev-erbA_WT
    '''
    # extract either "WT" or "Rev-erbA_WT"
    if inf.split("_")[1] == "WT":
        geno = inf.split("_")[1]
    elif inf.split("_")[1] == "Rev-erbA":
        geno = "_".join(inf.split("_")[1:3])
    else:
        print("Warning: second term should be WT or Re-erbA, found: %s" % inf.split("_")[1])
        return(None)
    # add replicate status if there is
    match = re.search("replicate", inf)
    if match:
        geno = '_'.join([geno, "repli"])
    return(geno)

def make_genos_dic(indir):
    '''
    Get dictionary for genos:
        {WT: WT, Rev-erbA_WT: Rev-erbA_WT, Rev-erbA_KO: Rev-erbA_KO, WT_repli: WT_repli}
    '''
    genos_dic = {}
    genos = set()
    for f in os.listdir(indir):
        if f.endswith(".bw"):
            genos.add(get_geno_from_fname(f))
    for g in list(genos):
        genos_dic[g] = g
    return(genos_dic)

def get_time_from_fname(inf):
    '''
    GRO-seq_WT_LiverZT16.bam.forward_SeqDepthNorm.bw -> ZT16
    or
    GRO-seq_WT_Liver_ZT1.bam.forward_SeqDepthNorm.bw -> ZT01
    or
    GRO-seq_Rev-erbA_WT_Liver_ZT10.bam.forward_SeqDepthNorm.bw -> ZT10
    '''
    match = re.search('ZT(\d+)', inf)
    if match:
        time = "%02d" % (float(match.group(1)), )
        return("".join(["ZT", time]))
    else:
        print("Warning: no match for ZTXX in %s" % inf)
        return(None)

def make_times_dic(indir):
    '''
    Get dictionary for times:
        {ZT01: ZT01, ZT04: ZT04, ZT07: ZT07, ZT10: ZT10, ZT13: ZT13, ... ZT22}
    '''
    times_dic = {}
    times = set()
    for f in os.listdir(indir):
        if f.endswith(".bw"):
            times.add(get_time_from_fname(f))
    for time in times:
        times_dic[time] = time
    return(times_dic)

def get_strand_from_fname(inf):
    '''
    GRO-seq_Rev-erbA_WT_Liver_ZT10.bam.forward_SeqDepthNorm.bw -> forward or reverse
    '''
    fwd_match = re.search("forward", inf)
    reverse_match = re.search("reverse", inf)
    if fwd_match:
        return(fwd_match.group())
    elif reverse_match:
        return(reverse_match.group())
    else:
        print("Warning: neither forward or reverse for %s:", inf)
        return(None)

def make_strands_dic(indir):
    '''
    Get dictionary for strands:
        {forward: forward, reverse: reverse}
    '''
    strands_dic = {}
    strands = set()
    for f in os.listdir(indir):
        if f.endswith(".bw"):
            strands.add(get_strand_from_fname(f))
    for strand in strands:
        strands_dic[strand] = strand
    return(strands_dic)

def main():
    parser = argparse.ArgumentParser(description='Make trackhubs for UCSC browser using bigWig files. Outputs to CURRENT DIRECTORY.')
    parser.add_argument('inputdir', metavar='INDIR',
                        help='Directory containing group1 and group, in which there are bigWig files .bw ending')
    parser.add_argument('hubname', metavar='HUBNAME', type = str,
                        help='Dirname which will be labelled and created')
    parser.add_argument('hubnameparent', metavar='HUBPARENT', type = str,
                        help='Parent directory to put trackhubs')
    parser.add_argument('--quiet', '-q', action='store_true',
                         help='Suppress some print statements')
    parser.add_argument('--render', '-r', action='store_true',
                         help='Render file to current dir')
    parser.add_argument('--upload', '-u', action='store_true',
                         help='Upload file to webserver')
    args = parser.parse_args()

    # store command line arguments for reproducibility
    CMD_INPUTS = ' '.join(['python'] + sys.argv)    # easy printing later
    # store argparse inputs for reproducibility / debugging purposes
    args_dic = vars(args)
    ARG_INPUTS = ['%s=%s' % (key, val) for key, val in args_dic.iteritems()]
    ARG_INPUTS = ' '.join(ARG_INPUTS)

    # Print arguments supplied by user
    if not args.quiet:
        print('Command line inputs:')
        print(CMD_INPUTS)
        print ('Argparse variables:')
        print(ARG_INPUTS)

    # define constants (hard coded)
    dirname = args.hubname
    srv_dir = os.path.join(args.hubnameparent, args.hubname)
    hubname = dirname
    shortlab = args.hubname
    longlab = "GROSeq %s" % args.hubname
    email = "jake.yeung@epfl.ch"
    # add main here
    url = "http://upnaesrv1.epfl.ch"
    assay = "GROSeq"

    # define URLs
    url_main = "%s/%s" % (url, srv_dir)
    url_base = "%s/%s/data" % (url, srv_dir)
    # upload_main = "~/Sites/%s" % dirname
    # upload_base = "~/Sites/%s/data" % dirname
    upload_main = "/data/web/sites/%s" % srv_dir
    upload_base = "/data/web/sites/%s/data" % srv_dir
    bigwigtype = "bigWig"
    host = "upnaesrv1.epfl.ch"
    user = "websync"

    # define constants
    genomebuild = "mm9"
    jaggregate = "transparentOverlay"
    jlimits = "0:100"
    autoscale = "on"

    # init hub genomes file genome trackdb
    # Make my hub
    hub = Hub(hub = hubname,
              short_label = shortlab,
              long_label = longlab,
              email = email,
              url = "%s" % url_main)

    hub.url = os.path.join(url_main, "%s.hub.txt" % hub.hub)

    genomes_file = GenomesFile()
    genome = Genome(genomebuild)
    trackdb = TrackDb()

    # add remote fn
    # hub.remote_fn = os.path.join(upload_main, "hub.txt")
    # genomes_file.remote_fn = os.path.join(upload_main, "genomes.txt")
    hub.remote_fn = upload_main
    genomes_file.remote_fn = upload_main
    trackdb.remote_fn = os.path.join(upload_main, genomebuild, "trackDb.txt")

    hub.add_genomes_file(genomes_file)
    genome.add_trackdb(trackdb)
    genomes_file.add_genome(genome)

    genos_dic = make_genos_dic(args.inputdir)
    times_dic = make_times_dic(args.inputdir)
    strands_dic = make_strands_dic(args.inputdir)

    add_aggregate(trackdb, args.inputdir, hubname, bigwigtype, url_base, upload_base, jlimits, autoscale, jaggregate, suffix="GROSeq")

    print 'Track looks like this:'
    print trackdb

    if args.render:
        print 'Rendering to %s' % hub.local_fn
        results = hub.render()

    if args.upload:
        print 'Uploading to web@circadian.epfl.ch'
        for track in trackdb.tracks:
            upload_track(track = track, host = host, user = user)
        upload_hub(hub = hub, host = host, user = user)

    print 'Hub URL if you uploaded to server: %s' % hub.url

if __name__ == '__main__':
    main()
