#!/bin/sh
# Jake Yeung
# run.make_trackhubs.sh
# Run trackhubs
# 2017-11-16

makescript="/Home/jyeung/projects/make_trackhub_scripts/make_trackhub_groseq_liver/make_trackhubs.py"
indir="/scratch/el/weekly/jyeung/groseq_liver/bigwigs_pos_neg_2.5.4"
[[ ! -d $indir ]] && echo "$indir not found, exiting" && exit 1

hubname="GROSeq_Liver_Fang"
hubparent="GROSeq_Liver"

python $makescript $indir $hubname $hubparent --render --upload

# do a hub check
hubf="/Home/jyeung/projects/make_trackhub_scripts/make_trackhub_groseq_liver/$hubname.hub.txt"
echo "Running hubCheck on $hubf"
hubCheck $hubf
ret=$?; [[ $ret -ne 0  ]] && echo "ERROR: hubCheck failed" && exit 1
# python $makescript $indir $hubname $hubparent --upload
